## Создание дашборда для Яндекс.Дзена

Нами была написан скрипт для выгрузки данных из базы данных и сохранения полученных данных в .csv файл для создания дашборда. 

Дашборд подготовлен на платформе Tableau Public. Кроме того, подготовлена презентация о дашборде и его основных частях.

[Презентация](https://docs.google.com/presentation/d/17E2EIgq4hYON29aU8PKJi-G8TrTZt_c2eMTZNo21L-w/edit?usp=sharing) 

[Дашборд](https://public.tableau.com/app/profile/evgenia4521/viz/YaPrBook/Dashboard1)

### Использованы библиотеки и методы:
- sqlalchemy
- Tableau Public
- Google презантации
